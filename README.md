# Agfa Snapscan e25 Scanner On Ubuntu 18.04 / 14.04 / 12.04 #


### Quick & dirty installation ###

* Clone the repo

```
#!shell

git clone https://tuxar@bitbucket.org/tuxar/snapscan-e25.git
```

* Run install-snapscan-e25.sh

```
#!shell

cd snapscan-e25
sudo ./install-snapscan-e25.sh
```

* Useful links
    * [SnapScan Backend for SANE](http://snapscan.sourceforge.net/)
    * http://manpages.ubuntu.com/manpages/trusty/en/man5/sane-snapscan.5.html
    * https://wiki.ubuntuusers.de/Scanner/Agfa_SnapScan/